// Initialize your app
var myApp = new Framework7({
    animateNavBackIcon: true,
    // Enable templates auto precompilation
    precompileTemplates: true,
    // Enabled pages rendering using Template7
	swipeBackPage: false,
	swipeBackPageThreshold: 1,
	swipePanel: "left",
	swipePanelCloseOpposite: true,
	pushState: true,
	pushStateRoot: undefined,
	pushStateNoAnimation: false,
	pushStateSeparator: '#!/',
    template7Pages: true
});

moment.locale('nb');


// Export selectors engine
var $$ = Dom7;

$$('.facebook-button').on('click', function (e) {
	window.open('https://www.facebook.com/131238610315878', '_system');
});

function openURL(urlString){
    window.open(urlString, '_blank');
}

document.addEventListener("deviceready", onDeviceReady, false);

onDeviceReady();

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

document.addEventListener("resume", onResume, false);

function onResume() {
	onDeviceReady();
}

var now = moment();

function onDeviceReady() {

	window.localStorage.clear();

    parseFeed(function(){
		$$.get('https://www.google.com/calendar/ical/cobraz.net_r4gl2gcg4vbil4g5qjiol18kes%40group.calendar.google.com/public/basic.ics', function(data){
			window.localStorage.setItem("moon15-ical", data);
			parseFeed();
		});
	});

    var mapPopup = myApp.photoBrowser({
	    photos : [
	        'http://s3.nconel.no/maanen/DONE.jpg'
	    ],
	    theme: 'dark',
	    navbar: true,
	    toolbar: false,
	    ofText: 'Kart over festivalområdet',
	    backLinkText: 'Tilbake',
	    type: 'popup'
	});

	$$('.map-popup').on('click', function () {
	    mapPopup.open();
	});

	$$('.popup-info').on('open', function () {
		$$.get('http://maanefestivalen.no/app-informasjon/?raw=true', function(data){
			$$('.infoContent').html(data);
		});
	});

	$$('.popup-artists').on('open', function () {
		$$.get('http://maanefestivalen.no/artister/?raw=true', function(data){
			$$('.artistsContent').html(data);
		});
	});

	$$('.panel-left').on('opened', function () {
		var scrollTopNow = $$('.panel-left').scrollTop();
	    if($$('#' + now.format('YYYY-MM-DD')) !== undefined && scrollTopNow == 0){
	    	var $title = $$('#' + now.format('YYYY-MM-DD')).find('h2');
	    	var $offset = $title.offset();
	    	if($$('#' + now.format('YYYY-MM-DD')).find('.finished') !== undefined){
	    		$offset = $$('#' + now.format('YYYY-MM-DD')).find('.notFinished').offset();
	    		$offset.top = $offset.top-200;
	    	}
	    	// :first

	    	$$('.panel-left').scrollTo(0, $offset.top, 500);
	    }
	});

}

function parseFeed(failedCallback) {
	var ical = window.localStorage.getItem("moon15-ical");

    if(ical == null){
        failedCallback();

    } else {

        var jcalData = ICAL.parse(ical),
        	vcalendar = new ICAL.Component(jcalData),
        	vevents = vcalendar.getAllSubcomponents('vevent')
        	count = vevents.length;

        var $title,
        	$dates,
			$locationTitle,
			$consertsList,
			$conserts,
			$dayElement,
			$locations;

		var $consertPanel = $$('.concert-panel');
		$consertPanel.html('');

		var $feed = [];

		$$.each(vevents, function(index, vevent){
			var location = vevent.getFirstPropertyValue('location');
			var dtstart = '' + vevent.getFirstPropertyValue('dtstart');

			var time = moment(dtstart);

			if($feed[time.format('YYYY-MM-DD')] == undefined){
				$feed[time.format('YYYY-MM-DD')] = [];
			}

			if($feed[time.format('YYYY-MM-DD')][location] == undefined){
				$feed[time.format('YYYY-MM-DD')][location] = [];
			}

			$feed[time.format('YYYY-MM-DD')][location].push({
				'summary': vevent.getFirstPropertyValue('summary'),
				'location': location,
				'time': time.format('HH:mm'),
				'moment': time
			});

			if (!--count) {

				$dates = Object.keys($feed);
				$dates.sort(function (a, b){
					return new Date(a) - new Date(b);
				});

				$$.each($dates, function(index, date){
					var day = capitalizeFirstLetter(moment(date).format('dddd D.'));

					$title = $$('<h2>').html(day);
					$conserts = $$('<nav/>').addClass('conserts');

					$locations = Object.keys($feed[date]);
					$locations.sort(function(a,b){
						if(b == 'Hovedscene') return 1;
					});

					$$.each($locations, function(index, location){
						$locationTitle = $$('<h3>').html(location);
						$conserts.append($locationTitle);

						$consertsList = $$('<ul/>');
						$feed[date][location].sort(function (a, b) {
						  return new Date('1970/01/01 ' + a.time) - new Date('1970/01/01 ' + b.time);
						});
						$feed[date][location].forEach(function(consert){
							var $item = $$('<li/>');
							$item.append('<strong>kl. ' + consert.time + '</strong> - ' + consert.summary);
								
							if(consert.moment.unix() < now.unix()){
								$item.addClass('finished');
							} else {
								$item.addClass('notFinished')
							}

							$consertsList.append($item);
						});
						$conserts.append($consertsList);
					});

					$dayElement = $$('<div/>');
					$dayElement.attr('id', moment(date).format('YYYY-MM-DD'));
					$dayElement.append($title);
					$dayElement.append($conserts);

					$consertPanel.append($dayElement);
				});
			}
			
		});

    }

}